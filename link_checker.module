<?php

//Link checker module to check on 404 links created by the link module (http://drupal.org/project/link)
//A large majority of this code was lovingly borrowed from the janode project at http://drupal.org/project/janode
//THANK YOU janode! 

define("_LINK_CHECKER_NOT_HTTP", 251);
define("_LINK_CHECKER_DATA", 252);
define("_LINK_CHECKER_FILE_OPEN_FAILURE", 353);
define("_LINK_CHECKER_FAILURE_PIVOT", 299);

/**
 * Implementation of hook_menu().
 */
function link_checker_menu($may_cache) {
  $items = array();
  if (!$may_cache) {
    $items[] = array(
      'path' => 'admin/settings/linkchecker',
      'callback' => 'drupal_get_form',
	  'callback arguments' => array('link_checker_admin_settings'),
	  'title' => t('Link checker'),
	  'description' => t('Configure link checker'),
      'access' => user_access('administer link checker'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function link_checker_perm(){
  return array('administer link checker');
}

/**
 * Administrator settings 
 *
 * @return Array for FAPI
 */
function link_checker_admin_settings() {

    //Check to see if allow_url_fopen is available, if not throw an error
    //so that the administrator knows that this will not work without it 
  if (!ini_get('allow_url_fopen')) {
    drupal_set_message(t('PHP allow_url_fopen is not enabled in order for this module to work it must be enabled! '));    
  }
    //Create the settings form
  $form = array(); 
  $form['link_checker_batch_quantity'] = array (
    '#type' => 'select',
    '#title' => t('Max links to check per link field'),
    '#description' => t('Determines the maximum number of links that will be checked for each link field per cron run. 
						 A high number will cause cron to run slowly whereas a low number will require cron to be ran more often. 
						'),
    '#options' => array(1=>1, 2=>2, 5=>5, 10=>10),
    '#default_value' => variable_get('link_checker_batch_quantity', NULL),
	);
  $form['link_checker_unpublish'] = array(
	'#type' => 'textfield',
	'#title' => t('Unpublish threshold'),
	'#description' => t('Number of consecutive CRON runs that detect an error to occur before unpublishing the related node, note, not all links are checked at every cron run due to the above setting. Enter 0 for never unpublish'), 
	'#default_value' => variable_get('link_checker_unpublish', 0),
	);

  //check for which nodes have link fields
  $types = array();
  foreach (content_types() as $node_type) {
    foreach ($node_type['fields'] as $field) {
	  if ($field['type'] == 'link') {
  	    //found a node type with a link field 
            $types[$node_type['type']] = $node_type['name'];
	  }	
	}
  }	

  $form['link_checker_node_types'] = array(
	'#type' => 'select',
	'#title' => t('Select the node types to check'),
	'#description' => t('Unselected items will not be checked for 404 errors'),
	'#options' => $types,
	'#default_value' => variable_get('link_checker_node_types', NULL),
	'#multiple' => true,
	);
	
	return system_settings_form($form);
}

/*
 * Implementation of hook_cron 
 */ 
function link_checker_cron() {
    //only search the node types the user wants to search through
  $node_types  = (array)variable_get('link_checker_node_types', NULL);
  foreach ($node_types as $node) {
    $fields = content_fields(NULL, $node);
    foreach ($fields as $field) {
	  if ($field['type'] == 'link') {
        //We're at the link field type if its multiple we'll query the link table directly 
        //otherwise we'll query the node type table directly
        if ($field['multiple'] == 1) {
	      $db_info = content_database_info($field);
          $table_name = $db_info['table'];
          $field_name = $db_info['columns']['url']['column'];
   		  $max = variable_get('link_checker_batch_quantity', 10);
	      $sql = "SELECT c.nid, c.vid, c.delta, c.$field_name as link
				  FROM {$table_name} c 
                  LEFT JOIN {link_checker} lc on c.nid = lc.nid AND c.vid = lc.vid AND c.delta = lc.delta  
                  WHERE c.$field_name <> ''
				  ORDER BY lc.last_checked ASC LIMIT %d
				";
		  $links = db_query($sql, $max);
		  while ($link = db_fetch_object($links)) {
			//send over to _link_checker_check_status to update the status in the DB
			$link->field_name = $field_name;
			_link_checker_check_status($link);
		  }
		}
		else {
		  //It is a field on the node type table
	      $db_info = content_database_info($field);
          $table_name = $db_info['table'];
          $field_name = $db_info['columns']['url']['column'];
   		  $max = variable_get('link_checker_batch_quantity', NULL);
	      $sql = "SELECT c.nid, c.vid, c.$field_name as link
				  FROM {$table_name} c 
                  LEFT JOIN {link_checker} lc on c.nid = lc.nid AND c.vid = lc.vid AND c.$field_name = lc.field_name
                  WHERE c.$field_name <> ''
				  ORDER BY lc.last_checked ASC LIMIT %d
				";
		  $links = db_query($sql, $max);
		  while ($link = db_fetch_object($links)) {
			//send over to _link_checker_check_status to update the status in the DB
			$link->field_name = $field_name;
			_link_checker_check_status($link);
		  }
		}
	  }	//if field['type']
	} //foreach fields as field	
  } //foreach node_types as node
}

/*
 * Checks the link for a status
*/
function _link_checker_check_status($link) {
  static $message_once = TRUE;
  // check php var "allow_url_fopen" is true as we need it to fetch the URL
  if (!ini_get('allow_url_fopen')) {
    if ($message_once) {
      watchdog('cron', 'PHP INI "allow_url_fopen" is false', WATCHDOG_NOTICE);
      $message_once = FALSE;
      return;
    }
  }
  
  static $link_threshold_check;

  //Code originally written in the janode project at http://drupal.org/project/janode, thanks! 

  // There are a number of HTTP status return codes. However, below 300 
  // usually means all went ok. We use 250 series for our own internal
  // error messaging. Our errors are non-fatal however, so are less than
  // 300. Status codes above 299 are considered errors and we force the
  // node back into the moderation queue for admin attention.

  $status = 0; // provide a default value to ensure var exists

  // create a full URL
  $url_parts = parse_url($link->link);
  if (isset($url_parts['port']) && strlen($url_parts['port']) > 0) {
    $url_parts['host'] .= ':' . $url_parts['port'];
  }
  $url_parts['path'] = (isset($url_parts['path'])) ? $url_parts['path'] : ('');
  $full_url = $url_parts['scheme'] . "://" . $url_parts['host'] . $url_parts['path'];
  if (isset($url_parts['query']) && strlen($url_parts['query']) > 0) {
    $full_url .= "?" . urlencode($url_parts['query']);
  }
  
  // currently only support http
  if ($url_parts['scheme'] != 'http') {
    $status = _LINK_CHECKER_NOT_HTTP;
  }
  else {
    if(!function_exists('stream_get_meta_data')) { // needed next
      $status = _LINK_CHECKER_NO_MATA_DATA;
    }
    elseif(!($fp = @fopen($full_url, 'r'))) { 
      $status = _LINK_CHECKER_FILE_OPEN_FAILURE;
      unset($_SESSION['messages']['error']); // rough but gets rid of hostname errors, @fopen didn't work
    }
    else {
      $meta_data = @stream_get_meta_data($fp);
      fclose($fp);
      if (is_array($meta_data['wrapper_data'])) {
        foreach($meta_data['wrapper_data'] as $v) {
          if (strtolower(substr($v, 0, 4)) == 'http') { // look for a server header starting "http"
            list($protcol, $status, $verbal) = explode(' ', $v); // and if found, assign to $status
            break;
          }
        }
      }
    }
  }

  // restore system error handler
  restore_error_handler();

  // tell the db what we have discovered...
  
  //First we need to check if there is an existing row 
  $sql = "SELECT lc.nid, lc.delta, lc.vid 
		  FROM {link_checker} lc
          WHERE lc.nid = %d AND lc.vid = %d AND lc.delta = %d AND lc.field_name = '%s' LIMIT 1";
		
  if (db_num_rows(db_query($sql, $link->nid, $link->vid, $link->delta, $link->field_name)) > 0) {
   //row exist so we'll update it.  
   $sql = "UPDATE {link_checker} 
            SET status = %s, last_checked = %d
            WHERE nid = %d AND vid = %d AND delta = %d AND field_name = '%s'
		   ";
    db_query($sql,$status, time(), $link->nid, $link->vid, $link->delta, $link->field_name);
  }
  else {
	//row doesn't exist so we'll add it in
	$sql = "INSERT INTO {link_checker} (nid, vid, delta, last_checked, status, field_name) 
            VALUES (%d, %d, %d, %d, '%s', '%s') ";
    db_query($sql, $link->nid, $link->vid, $link->delta, time(), $status, $link->field_name);
  }

  // this seems to execute more than once for the same field, so better be safe
  if(@!isset($link_threshold_check["{$link->vid}-{$link->delta}"])) {
    // unpublish and force the node into the moderation queue 
    if ((int)$status > _LINK_CHECKER_FAILURE_PIVOT && variable_get('link_checker_unpublish',0) >0) {
  	//The user would like all errors above 300 unpublished.. ok we'll do that. 
      db_query("UPDATE {link_checker} set error_count = error_count+1 where vid=%d and delta =%d",$link->vid, $link->delta );
      $result = db_query("SELECT error_count from {link_checker} where vid=%d and delta =%d",$link->vid, $link->delta );
      $stat = db_fetch_array($result);
      if($stat['error_count'] >= variable_get('link_checker_unpublish',0)) {
        // @todo you could work in someting from actions module here instead?
        db_query("UPDATE {node} SET status = 0, moderate = 1 WHERE nid = %d", $link->nid);
      }
    } else {
      // we dont care for, or we need to reset the error_count
      db_query("UPDATE {link_checker} set error_count = 0 where vid=%d and delta =%d",$link->vid, $link->delta );
    }
  }
  $link_threshold_check["{$link->vid}-{$link->delta}"]=true;
}
